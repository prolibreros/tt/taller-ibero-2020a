# ¿Cómo hacer mis archivos?

Tu artículo está escrito en el servidor de Colima Hacklab. Puedes
acceder [aquí](tutifruti.softwarelibre.org.mx:3000).

Los procesos que tienes que hacer son los siguientes:

1. Ingresar y copiar la URL de tu pad
2. Descargar el MD en tu carpeta
3. Generar el EPUB con Pecas
4. Generar el PDF con `export-pdf`

¡Empecemos!

## 1. Ingreso y copia de la URL

El servidor CodiMD de Colima Hacklab es: http://tutifruti.softwarelibre.org.mx

Si estás como invitado, ve a la ventana de «Historia». Si tienes
usuario, automáticamente te redirige a esa sección.

A continuación da clic sobre tu artículo y espera a que se cargue
el pad.

Ahora ya puedes copiar la URL pero ¡ten cuidado! Solo tienes
que copiar hasta el ID de tu pad, por ejemplo:

```
http://tutifruti.softwarelibre.org.mx:3000/hvA1W0lxSJGpQNJJzpTbIA
```

Si te fijas la URL está compuesta por:

* Un protocolo: `http://`
* Un nombre de subdominio: `tutifruti.`
* El nombre de dominio: `softwarelibre.org.mx`
* Un puerto: `:3000`
* Un ID: `/hvA1W0lxSJGpQNJJzpTbIA`

Si además de estos elementos tu URL tiene al final una almohadilla
(`#`) o un signo de interrogación y a continuación texto (por
ejemplo, `?both`), __tienes que eliminarlo__. De lo contrario,
no podrás descargar el archivo.

2. Descarga de MD

Con la URL lista y correcta solo basta ejecutar el siguiente
comando desde el servidor SSH de tutifruti:

```
wget http://tutifruti.softwarelibre.org.mx:3000/TUID/download -O ~/taller-ibero-2020a/trabajos/USUARIO/archivos-madre/md/todo.md
``` 

Nota que `TUID` corresponde al identificador de tu pad que previamente
copiaste en el paso 1. Mientras tanto `USUARIO` es el nombre
de tu carpeta de usuario, por lo cual lo tienes que sustituir
por el correcto, de lo contrario __no funcionará__.

> ¿Qué es un servidor SSH? SSH es Secure SHell
> y es una tecnología que permite acceder a
> computadoras remotas desde la terminal. Al
> hacer `ssh ted@orbilibro.mx` o poniendo esta
> información con PuTTY, estás ya accediendo
> a través de un servidor SSH.

Una vez descargado el archivo, ¡podemos continuar!

## 3. Generación de EPUB

Para generar el EPUB primero tenemos que irnos a la carpeta del
proyecto EPUB, esto es posible si ejecutamos:

```
cd ~/taller-ibero-2020a/recursos-comunes/epub-automata
```

Ahora hacemos el EPUB con este comando:

```
pc-automata --no-analytics --no-ace --overwrite -f ../../trabajos/USUARIO/archivos-madre/md/todo.md
```

De nueva cuenta, sustituye `USUARIO` por tu nombre de _fichero_
de usuario, de lo contrario __no funcionará__.

Este comando invoca a Pecas para la producción de un EPUB y un
MOBI sin analítica (el servidor está teniendo problemas con hacerla),
sin verificación para personas con debilidad visual y sobreescribiendo
los archivos ya existentes (que no es un problema porque los
archivos principales los tienes en `archivos-madre` :).

Por último, ¡movamos los archivos a tu carpeta `out`!:

```
mv *.epub ../../trabajos/USUARIO/out && mv *.mobi ../../trabajos/USUARIO/out
```

Otra vez, ¡no olvides sustituir `USUARIO` por tu nombre de usuario!

Con esto ya estamos listo para el último paso
>:)

> ¿Qué es un _fichero_? La palabra fichero
> se utiliza como sinónimo para cualquier tipo
> de documento o directorio de tu computadora.
> Así que puede significar una u otra cosa xd

## 4. Generación de PDF

Lo último que nos queda es exportar nuestro MD a formato PDF,
para evitar cualquier error, ¿qué te parece si antes nos vamos
a tu carpeta `out`?:

```
cd ~/taller-ibero-2020a/trabajos/USUARIO/out
```

Parece disco rayado, pero no olvides sustituir `USUARIO` por
tu nombre de usuario.

Ahora solo ejecutamos:

```
export-pdf --title="TÍTULO" --author="NOMBRE" --ragged-right --imposition ../archivos-madre/md/todo.md
```

Cambia la siguiente información:

* `TÍTULO`: el título de tu artículo
* `NOMBRE`: tu nombre

Además de esto, estamos usando la opción `--ragged-right` para
tener una alineación «a bandera». La alineación justificada por
defecto (pueden obtenerla si ejecutan sin el parámetro `--ragged-right`)
no es noble para nuestra vista, buuu.

Por último mueve los documentos a tu carpeta `out` con:

```
mv ../archivos-madre/md/*.pdf .
```

En tu carpeta `out` tendrás que tener un archivo PDF y otro más
con composición, ¡listo para imprimir un folleto en tu impresora.

Con esto termina el ejercio :O

## Bye

Como vieron, usar nombres y estructuras uniformes y consistentes
en los archivos nos permite controlar los flujos de trabajo,
hasta el punto que los comandos indicados sean igualmente aplicables
para todos.

¡Bye!
