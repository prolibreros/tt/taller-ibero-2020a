# La Maternidad en Simone de Beauvoir y Rosario Castellanos

## Abstract

En discusiones filosóficas en torno a la maternidad, un asunto controversial ha sido si la maternidad determina inevitablemente a las mujeres, restringiendo su libertad. Por un lado, Simone de Beauvoir refiere a la maternidad como aquello que vuelve a las mujeres un contenedor pasivo. Por otro lado, está Rosario Castellanos, quien apela a que, si se afirma como un sentimiento consciente, la maternidad propicia que las mujeres satisfagan su necesidad de “eternizarse”. En este artículo, me concentraré en analizar, comparar y contrastar estos dos planteamientos en la maternidad: el pensamiento de Rosario Castellanos en Sobre cultura femenina y el de Simone de Beauvoir en El Segundo Sexo. Mi objetivo es reunir elementos para pensar en la maternidad como un no-obstáculo. En otras palabras, como un espacio de indeterminación, un ámbito de posibilidad justo en el encuentro con el obstáculo que en cierto sentido, la maternidad también es.    

Palabras clave: maternidad, indeterminación, Simone de Beauvoir, Rosario Castellanos

## Introducción

La pugna por el aborto legal y gratuito en México reabre testimonios que rompen con la imagen de una maternidad feliz y estática que completa la satisfacción de las mujeres. En estos testimonios se llega a decir, aunque amen a sus hijos: “si pudiera retroceder el reloj, no tendría hijos”.^1^ Tal afirmación pone en juicio los referentes asociados con la maternidad. Por ejemplo, comienza la duda en torno a creencias populares de que la maternidad desarrolla las hormonas necesarias para que las mujeres se sientan satisfechas por el resto de sus vidas o que la maternidad propicia las condiciones materiales para que las mujeres dejen atrás la categoría de “niña” y, así, se sientan plenamente libres.

Así, la maternidad, todavía relevante, adquiere un cuestionamiento más allá del social o del biológico y presume un posible acercamiento filosófico. En ese acercamiento la maternidad se comprendería como un estado en el que la gestación se puede presentar y en el que el cuidado de un infante se demanda. La gestación si bien es un fenómeno físico que repercute en la psique de la embarazada, también se presta para preguntar si la maternidad puede experimentarse dentro de algún sentido ontológico (además de óntico). Por su parte la demanda del cuidado de los hijos tiene su parte social e histórica; mas también permite cuestionar cuál es su relación con la libertad. 
	
Este artículo indagará si la maternidad predetermina (por medio de la experiencia corporal), y en qué sentido, a las madres. Es decir, se preguntará si las madres durante la gestación y el cuidado de los hijos están en un espacio predeterminado que elimina sus posibilidades de emancipación. O por el contrario, permite (de cierto modo) un espacio de indeterminación en el que la madre ejerce un acto creativo. Aunque, no por ello, este ámbito de posibilidad eliminaría el encuentro con el obstáculo que la maternidad, de alguna manera, representa. 

Para fines de este texto, la “determinación” se entenderá como aquello que ata todas las propiedades de algo,^2^ en este caso alguien (una mujer), a un único estado posible (la maternidad). Por lo que la pre-determinación, partiendo de la determinación, ataría las propiedades de una cosa a un estado específico, en toda ocasión y de manera absoluta. Es decir, no cabría trasgredir la atadura y cambiar de estado a la cosa. Por lo que, en este caso, si la maternidad de cualquier mujer estuviera predeterminada, no poseería ninguna propiedad independiente que diferenciara su experiencia del resto de las experiencias de maternidad. Por ello, una maternidad como espacio de indeterminación correspondería a la posibilidad de experimentar la maternidad en la multiplicidad y en la creación. En resumen, la maternidad como “pre-determinada” sería un modo o conjunto de modos únicos de la experiencia materna que son referidos, o no, a la libertad (subrayando la parte ética y no sólo ontológica de ella).

Así, este artículo intentará articular las lecturas del _Segundo Sexo_ que interpretan a la maternidad como un obstáculo con aquellas que la perciben más positivamente con respecto de la emancipación femenina (libertad de modos de experiencia). Además, se pretendería agregar, a esa articulación, la noción de la maternidad como un acto creativo de Rosario Castellanos. Dado que el acto creativo tendría como base una cierta libertad que, si bien, puede reducirse por circunstancias externas de la madre, también supone indeterminación para crear. Se apuesta por que la maternidad no necesita ser un espacio predeterminado, sino que las madres tienen la posibilidad de afirmarse a sí mismas y de ser libres en ella, siempre que se tome como acto creativo. 

Se busca conjuntar algunas premisas de Simone de Beauvoir con otras de Rosario Castellanos debido a que son autoras con pensamientos feministas distintos (aunque con algunos encuentros), con distancia geográfica, pero no temporal. Es decir, Simone de Beauvoir, desde Francia, escribe a partir de las políticas públicas reproductivas remanentes de la Comuna de París que castigaban duramente los abortos y el uso de anticonceptivos.^3^ Beauvoir publicó _El Segundo Sexo_ en 1949, cuando el aborto continuaba siendo ilegal; pero la pena de muerte se había suprimido de su sanción.^4^ Por su parte, Rosario Castellanos presentó su tesis de maestría, _Sobre cultura femenina_, en 1954. Lo que da un espacio de cinco años entre ambas publicaciones. Castellanos, por su parte, redactó su texto cinco años antes de que se legalizara el sufragio femenino en México y, con ello, inauguró la liberación intelectual de las mujeres en México, de acuerdo con Marta Lamas.^5^ Derecho que ya se había ganado en Francia.^6^

Pero ¿cómo se introduce la maternidad en los dos textos? Por un lado, _El Segundo Sexo_ desde el existencialismo analiza cómo la condición femenina se distancia de los aspectos emancipatorios, no por la diferencia de sexos, sino por las condiciones históricas y materiales en las que se inscriben las mujeres reduciendo su igualdad de condición frente a los hombres. La maternidad, con ello, se presenta como un fenómeno inmanente. Por lo que la libertad se ve en la dificultad de plantearse conjuntamente con la maternidad. Así, luce plausible afirmar que la emancipación debe ser transcendente de manera que se continúe superando las metas que uno mismo se ha impuesto. Por otro lado, _Sobre cultura femenina_ se pregunta si existe algo llamado “cultura femenina”, es decir, si hay una creación cultural de las mujeres que produce paralelamente a la creación cultural dominada por los hombres. Castellanos concluye que la exclusión histórica de las mujeres de la cultura, las ha forzado a crear sólo en la maternidad. Por lo que la maternidad, retomando el lenguaje de Schopenhauer, trasciende su “espíritu” hacia la “eternización”. Por lo que, parece propicio conjeturar que, también, Castellanos entiende la libertad referida a la trascendencia. Sin embargo, a distinción de De Beauvoir, la maternidad sí es trascendente.

Beauvoir, en _El Segundo Sexo_, destaca que hay sentimientos encontrados durante la gestación. Pues, por un lado, el feto es parte de ella misma y, por el otro, es un ente parasitario que se alimenta de ella. La madre siente que debe amar incondicionalmente al feto aunque, a su vez, lo repudia. Además, Beauvoir postula la relación de la maternidad con el ámbito social de la madre como el momento en el que ella debe sobreponer la voluntad de su hijo por encima de la suya. Así, las propuestas de Beauvoir, dan la impresión de que la maternidad se experimenta infeliz, como un un obstáculo. Aunque, no por ello, se da una determinación total de cómo se experimenta la maternidad; puesto que Beauvoir también la refiere a la contingencia y al cambio.

En contraste, Rosario Castellanos en Sobre cultura femenina, partiendo del sistema de Schopenhauer, sostiene que la maternidad ha sido la manera en la que las mujeres han podido afirmarse a sí mismas porque a lo largo de la Historia han sido excluidas de la creación cultural. Las mujeres, en la necesidad de afirmarse, se apropiaron conscientemente de su función reproductiva como su modo originario y auténtico de crear.^8^ En este punto, Castellanos compara la maternidad con la creación cultural masculina.

Cabe destacar que, pesar de que los parámetros de Castellanos son constituidos para reponer la exclusión de la creación, dan la sensación de esencializar la maternidad como el modo más propio y auténtico de las mujeres. Incluso, Castellanos propone que otras formas de creación “apartan” a la mujer de su feminidad, “confinándola a una mimetización del varón”.^9^ Por lo que es imperante, para Castellanos, que la maternidad sea experimentada de modo trascendente y creativo, adoptando una posición experiencial cerrada.

Sin embargo ¿Beauvoir sólo ve en la maternidad un obstáculo para las mujeres? ¿Castellanos sólo la contempla como un acto creativo? En un intento de respuesta, el artículo se dividirá en cuatro apartados que revisarán sucesivamente: 1) una lectura del discurso sobre la maternidad en _El Segundo Sexo_, 2) un análisis de las interpretaciones de ese discurso que lo ubican como una percepción determinista de la maternidad, 3) una lectura del discurso sobre la maternidad en _Sobre cultura femenina_ y 4) una discusión de las dos posiciones que apunta hacia una reinterpretación de la maternidad desde la indeterminación y la creación. 

## Leyendo _El Segundo Sexo_

El capítulo “La madre”, de El Segundo Sexo, tiene el objetivo específico de reflexionar sobre la condición femenina compartida de la maternidad (desde el embarazo, ya sea deseado o no, hasta la separación de la madre con los hijos adultos). El análisis se construye a partir de los testimonios femeninos que Beauvoir recolectó de libros sobre sociología o psicología y de relatos que ella leyó o de anécdotas que escuchó. Según Celia Amorós, el método del Segundo Sexo es el regresivo progresivo; el cual consta, tal como Amorós argumenta, de una “forma sistemática a psicoanálisis existenciales de colectividades específicas”.^10^ Ella explica que este psicoanálisis dista del clásico freudiano porque incluye la clase social de los sujetos y entiende la existencia en tanto que “tiene su consistencia fuera de sí, no es una esencia” y que es un proyecto. Es decir, “ha de transcenderse, ir más allá de sí hacia fines y objetivos que libremente se propone”.^11^ De este modo, la libertad referida por Beauvoir (según Amorós) no es abstracta y se da en una determinada situación.^14^ Así, pues, el análisis beauvoiriano de la existencia femenina “trata de identificar lo que tienen en común las situaciones”^15^  de las mujeres (como colectivo) para dar cuenta de su “condición femenina”.^16^ Así, Simone de Beauvoir escribió sobre “la naturaleza en general de la vida de las mujeres”;^17^ subrayando cómo la sumisión de la mujer ante el dominio masculino es aquello que la reduce a su cuerpo y que esencializa su identidad en roles como el de la madre. En otras palabras, los cuerpos femeninos son definidos por otros que no son ellas.^18^

Para iniciar una propia lectura del Segundo Sexo, retomaré además el concepto de “experiencia vivida” detallado por Sandra Reineke, para quien El Segundo Sexo (como parte de la literatura del existencialismo francés) expande “el concepto existencialista de «experiencia vivida» para incluir las experiencias de corporeidad que las mujeres comparten, aunque con diferencias”.^19^ De este modo la filosofía beauvoriana, para analizar la maternidad, describe minuciosamente las situaciones compartidas por las mujeres europeas. A su vez, para fines de este artículo, las situaciones serán comprendidas bajo la clave de interpretación de Teresa López Pardina, quien refiere a que las situaciones específicas, como la clase social, ofrecen la capacidad de aumentar o disminuir qué tan libre se comporta un sujeto. Esta manera de comprender las situaciones implica que los sujetos (las mujeres) actúan bajo la circunscripción de la situación a la que están inscritas.^20^

“La madre” comienza analizando la relación de las mujeres con los métodos anticonceptivos y la preponderancia del acceso al aborto. Parece que, con esos dos tópicos, Beauvoir presenta que la maternidad debiese ser una elección, no un deber, una no obligación y, mucho menos, un castigo de la sexualidad femenina. Posteriormente, Beauvoir trata la gestación, tanto en su recibimiento feliz, como en su experimentación penosa. Continúa con la crianza de los hijos y cómo socialmente se apremia a la madre a volcarse hacia los designios de sus vástagos, olvidándose de su propia subjetividad. Después se enfoca en cómo las madres interactúan con sus hijos durante su crecimiento. Por último, destaca cuáles son los impedimentos de la maternidad, es decir, los obstáculos que las madres afrontan en su contexto durante los cuidados.

Aquí, me concentraré en los argumentos sobre: 1) la gestación y los sentimientos ambiguos derivados de ella, 2) la crianza referida a atender la subjetividad del infante por encima de la de la madre, 3) el modo en que la madre responde al embarazo y a sus hijos y 4) las limitaciones sociales de la maternidad.

Así, pues, primeramente “La madre” introduce la consideración de que las mujeres experimentan su embarazo de manera contradictoria. Porque, por un lado, la maternidad se percibe por ellas como un enriquecimiento acorde con su función reproductiva, en tanto que “destino biológico”.^21^ Pero, por el otro lado, las madres sienten que se han mutilado a sí mismas.^22^ En tanto que, desde ese momento, pasarán a ser un contenedor pasivo de otro ser que puede entenderse como una criatura totalmente distinta a su madre o como una extensión de ella misma.^23^ Beauvoir rescata esta experiencia de vida a partir de la narración de Sofía Tolstoi:

> el embarazo es, sobre todo, un drama que se representa en el interior de la mujer; […] el feto es el embarazo es, sobre todo, un drama que se representa en el interior de la mujer; […] el feto es una parte de su cuerpo y es también un parásito que la explota; ella lo posee y también es poseída por él; […] al llevarlo en su seno, la mujer se siente vasta como el mundo; pero […] tiene la impresión de no ser ya nada.^24^

Esta contradicción del embarazo termina comúnmente en la pasividad de la mujer. La pasividad se comprende como la imposibilidad de existir para y por una misma. Por ello, las madres o futuras madres, gracias a su hijo, se volverán pasivas y se les impedirá la afirmación de sí mismas. En otras palabras, la maternidad no es una situación plenamente libre, sino que queda inscrita bajo la situación específica de cada mujer. Por ello, ellas gestan dentro de “la contingencia y la facticidad”.^25^ Ella no decide cómo será el humano que nazca; el niño se hace en ella sin su intervención consciente.^26^ Sin embargo, su maternidad sí la define a ella. El hijo justifica la existencia de la mujer porque ella es sierva de la nueva generación humana, no vive para sí ni por sí: es un “ser-para-el-otro”.^27^ Pero, quien la define, en su maternidad, es el hombre: “[l]a mujer […] modelará a menudo sus sentimientos de acuerdo con lo que él siente, y recibe entonces el embarazo o la maternidad […] según él se sienta orgulloso o fastidiado”.^28^ El hijo no define a la madre porque es una “consciencia balbuceante […] el niño no tiene en sí ningún valor y no puede conferir ninguno”.^29^ Y ella tampoco puede auto-afirmarse debido a que no decide, enteramente, por sí mismas la manera de ejercer su maternidad.

Beauvoir advierte que la maternidad, definida por el padre de la criatura, tiende a reducir a las mujeres a madres, a cuidadoras del otro. Postula un modo de experimentar la maternidad por encima de otros; dado que, aun si ellas desean a sus hijos, es más que probable que su cuidado signifique la adquisición de una nueva limitación: “[…] porque es un ser humano [la mujer], consciente y liberado, que se ha convertido en pasivo instrumento de la vida”.^30^ En otras palabras, quien sabe su potencial, cuando es reducida a contenedor de la futura generación humana, se le priva su emancipación. Ya no tiene la posibilidad de su libertad, únicamente queda dispuesta al servicio de aquella nueva vida. En el prólogo a la edición española de El segundo sexo, Teresa López Pardina explica que, para De Beauvoir, la libertad se presenta como una superación constante de aquello que el sujeto es y en el planteamiento de nuevos proyectos. Es decir, para Beauvoir de acuerdo con López Pardina, la libertad se da en la facticidad: “sólo me realizo como libertad superando constantemente lo que soy y alcanzando nuevas libertades desde la que haré nuevos proyectos”.^31^

Pero ¿en qué sentido la maternidad también reduce a las mujeres que desean ser madres? Siguiendo la lectura de “La madre”, parece que las “experiencias vividas” contradictorias son parte de la respuesta. Por ejemplo, las mujeres, con su psique tensa entre la afirmación de ellas mismas como entes libres y la inclinación de su persona hacia el cuidado del hijo, se provocan sus malestares físicos propios de la gestación, como lo es la náusea.

Beauvoir explica que la experiencia vivida contradictoria de la maternidad no culmina en el embarazo. Prosigue en el alumbramiento; aunque toma nuevos síntomas en el parto y otros durante la lactancia y el cuidado posterior. Primeramente, los mareos, las náuseas y los antojos asociados a la gestación son un síntoma de cierto rechazo hacia el embarazo. Es decir, los malestares físicos de las mujeres embarazadas son provocados por sentimientos encontrados (ninguna otra especie animal los presenta). Por ejemplo, Beauvoir relata la confesión (recopilada por Stekel) de una señorita encinta que padecía constantes vómitos y prolongado estreñimiento. La señorita adjudicaba el origen de su malestar a que, por un lado, con el vómito deseaba un aborto bucal. Por el otro lado, con el estreñimiento intentaba retener fuertemente al feto dentro de ella.^32^

En segundo lugar, los alumbramientos, también contradictorios, se dividen (mayormente) en dos grupos: 1) los prolongados y dolorosos y 2) los rápidos que requieren poco esfuerzo. Beauvoir apunta a que los primeros suelen dar cuenta del temor de las mujeres de perderse a sí mismas con la llegada del nuevo bebé. La mujer teme dejar de lado su autonomía (olvidarse de sí misma) para dedicarse al cuidado del recién nacido: 

>[e]l parto tendrá un carácter muy diferente según los casos; la madre quiere conservar en su vientre el tesoro de su carne que es un precioso trozo de sí misma, y quiere liberarse a la vez de un malestar; quiere tener, al fin, su sueño entre las manos, pero teme las nuevas responsabilidades que va a crear esa materialización; uno u otro deseo pueden imponerse [...]

>Para ciertas mujeres el parto es un martirio. Tal es el caso de Isadora Duncan, que había vivido su embarazo en la angustia y de los dolores cuyo parto se vieron agravados, sin duda, por una serie de resistencias psíquicas. [anteriormente Beauvoir había subrayado que Duncan vivió su embarazo como una disminución de su Yo]^33^

Los partos menos dolorosos normalmente se explican cuando la mujer deja que su cuerpo actúe sin pensar. Ella no se siente partícipe del proceso, su cuerpo responde solo y, por consiguiente, la mujer ofrece poca resistencia y hay poco dolor. Así, las mujeres del segundo grupo, las ponedoras (como las llama Beauvoir), se sienten muy cómodas afirmándose únicamente en su cuerpo y en la posición social adjudicada a la maternidad. Por lo que no se sienten amenazadas por el nuevo hijo. Consideran la maternidad como algo meramente corporal y natural.^34^

En tercer lugar, la lactancia, a veces, acarrea la sensación de una maternidad completa. En tanto que la madre ahora tiene entre sus brazos a una criatura diferente de ella. Durante la lactancia, las mujeres pueden disfrutar la justificación social de sí mismas porque, culturalmente, la madre ya no necesita ser sirvienta directa del hombre. Ahora atiende, sin recompensa, a la nueva generación humana. Por lo que se ha convertido en un ejemplo a seguir. No obstante, también existe la experiencia de enajenación de la madre ante su hijo lactante. Todavía más, Beauvoir recuerda que hay madres que rechazan a sus niños explicando que las lastiman con sus mandíbulas al succionar durante el amamantamiento.

Pero ¿qué tienen en común las distintas experiencias vividas de la gestación, del parto y de la lactancia con respecto de la noción de maternidad como un obstáculo? Para Beauvoir, parece residir en que las madres, aunque sean alabadas socialmente, siguen sin tener injerencia en su mundo. Su entorno no es dictado por ellas, sino por los hombres que las rodean y las definen. Empero, sí influyen en lo que les sucede a sus hijos. Incluso si los hijos “tienen derecho sobre ellas”,^35^ las madres pueden dominarles. Por lo que sólo pueden interceder en lo que le sucede al hijo: “ella capta en él [el hijo] lo que el hombre busca en la mujer: un otro, naturaleza y consciencia a la vez que sea su presa, su _doble_”.^36^ En este punto, quizá sea factible conjeturar que la mujer, aunque quede su libertad cercada por las condiciones que establece la maternidad en determinadas situaciones, posee cierta capacidad de dominación sobre sus vástagos. Pues, parece que, desde la visión en la que el hijo fue [_¿es?_] parte de la mujer durante la gestación, le pertenece en tanto que necesita de ella para su supervivencia o bienestar. Sin embargo, esta dominación sobre los hijos, no se compara con la de los hombres sobre las mujeres porque la situación de los hijos parece no determinarse a partir de las madres. Sino que la situación de los hijos es una elongación de la de las madres.

Todavía con respecto de la injerencia materna, Beauvoir declara que el cuidado del otro, más que ser un acto conscientemente elegido, se realiza por costumbre.^37^ De modo que, al crecer los hijos, las madres pierden su justificación social al ya no ser necesarias en tanto que cuidadoras.^38^ Parece que Beauvoir queda escéptica con respecto de que la mujer pueda fundar por sí misma su existencia a partir de la reproducción y el cuidado. Incluso ella declara que la mujer relegada al hogar “no tiene los medios para afirmarse en su singularidad, y por lo tanto ésta no le es reconocida [fundación de su existencia por sí misma]”.^39^ En este punto, con esta aseveración, a lo mejor es plausible preguntarse si Beauvoir encuentra posibilidad de libertad en la maternidad o si sólo es un inconveniente, ya sea por motivos sociales o por una cuestión más ontológica. Pues, la respuesta a tal cuestión, luce bastante sutil y, por lo mismo, poco evidente.

Quizá, la maternidad como obstáculo tenga condiciones materiales e históricas tan arraigadas y antiguas que hacen creer que su impedimento es ontológico. Sin embargo, de ser ontológico la experiencia materna tendría un único modo y quedaría por definición determinada y, por consiguiente, suspendida de libertad.

## El obstáculo de la maternidad: lecturas del _Segundo Sexo_

Algunas lecturas feministas del _Segundo Sexo_ consideran que, para Simone de Beauvoir, la maternidad se reduce a un impedimento para la emancipación femenina. Esta línea de interpretación tiende a concentrarse en la afirmación beauvoiriana sobre la vivencia contradictoria que las mujeres tienen con su embarazo y que propicia una experiencia materna desastrosa (referida a la infelicidad). Por ejemplo, Linda Zerilli llega a afirmar, desde su lectura del Segundo Sexo, que “el horror retórico de Beauvoir [sobre el cuerpo materno], de «entidades parasitarias» invasivas y el «crecimiento celular»”^40^ es el drama de la maternidad y el horror del cuerpo materno. Lo cual, es problemático porque, en cierto sentido, hay un rechazo al cuerpo femenino que se aparta (gestando) de lo que visualmente se ve más masculino. De manera que este rechazo corporal guardaría un poco de dominación masculina con respecto del símil visual de la emancipación femenina.

Zerilli encuentra cierto horror desde la visión beauvoiriana hacia el cuerpo embarazado, es decir, es “una imagen de pesadilla”^41^ la silueta de una embarazada. Este horror retórico articula una “ansiedad existencialista”^42^ frente a la pérdida de autonomía de la mujer con su nuevo cuerpo abultado y gestante. En otras palabras, en un cuerpo embarazado la agencia femenina decrece y, por ello, ella se aterra. La futura madre (al decidir dar a luz) queda sujeta a los designios del feto y del infante. Parece ser que este horror retórico (siguiendo la lectura de López Pardina) es existencialista porque disminuye el alcance de la libertad para las mujeres en tanto que el embarazo, en esta situación particular, no aumenta la posibilidad de las mujeres de comportarse lo más libres posible.^43^ De esta forma, las mujeres embarazadas se vuelven un contenedor pasivo, es decir,  se les impide su libertad porque la agencia de la embarazada, tanto en definición de su vida, como en su contexto dependen de algo que no es ella.^44^ En resumen, la madre se posiciona por debajo de su feto o su hijo y renuncia a su capacidad de agencia con tal de cumplir los deseos y obligaciones que social e históricamente adquiere con la maternidad.

Cabe destacar que en su texto, Zerilli intenta dar cuenta de una maternidad que no pretenda mantener a un sujeto autónomo femenino enfrentado con el hecho de ser madre. De este modo, para su análisis ella compara su lectura del _Segundo Sexo_ con la que hace de “Women’s Time” de Julia Kristeva y, aunque Zerilli parece inclinarse hacia el pensamiento de Kristeva, no deja de defender una lectura matizada del _Segundo Sexo_.

Otro ejemplo de esas lecturas feministas que atribuyen a Beauvoir un rechazo tajante hacia la maternidad sería la de Horner.^45^ De acuerdo con él, la filosofía beauvoiriana, en la constitución de la mujer embarazada como un “yo”, continúa con los vínculos tradicionales de la dialéctica, es decir, refuerza las “actitudes subjetivas y objetivas”.^46^ Esto se debe a que en el cuerpo de la embarazada hay algo que la hace formar una actitud subjetiva enfrentada con una objetiva. Porque, en la lectura que Horner hace del _Segundo Sexo_, en el cuerpo materno confluyen, en un mismo espacio que es ella, dos entes distintos y con lo que parece ser una subjetividad diferente. Así, la mujer embarazada es, a la vez, ella misma y otro. Según Horner, en _El Segundo Sexo_, el propio ser de la mujer se divide causando que la experiencia materna sea más negativa que “la co-presencia de la vida y la muerte”^47^ referidas al parir. Para Horner, la co-presencia de la vida y la muerte se estipula con la posibilidad de la madre, del hijo o de ambos de morir en el parto o de sobrevivir. Horner apostaría que la experiencia de ser un sujeto escindido se vive más negativamente que la posibilidad de morir dando a luz. Por lo que, para Horner, su crítica hacia la filosofía del _Segundo Sexo_ es que no deja claro si el feto es autónomo, o no, dentro del cuerpo de su madre.

Con este segmento, no se pretende desvalorizar ninguna lectura del Segundo Sexo, sino mostrar que algunas se acercan más hacia la noción de que la maternidad, para Beauvoir, reduce la potencia de las mujeres con respecto de la emancipación femenina. Lo cuál, por un lado, es cierto. Aunque quizá, se podría matizar todavía más esta posición. Las interpretaciones del _Segundo Sexo_ de Zerilli y de Hornet se inclinan más hacia una maternidad que, en este artículo, sería predeterminada. Sin embargo, con la propia lectura del texto de Beauvoir pretendo poner en cuestión qué tan vehementemente sería la declaración de la maternidad como una reducción de la potencia de las madres dentro del mismo escrito. 

Por otro lado, a pesar de que es discutible afirmar que Beauvoir considera que el impedimento de la emancipación femenina, en la maternidad, sólo reside en percances sociales;^48^ sí se presenta, en las descripciones de casos reales en El Segundo Sexo, la presión social e histórica sobre la mujer para asumirse como madre a expensas de sí misma. Por consiguiente, sería pertinente seguir la lectura de Alison Stone, en tanto que Beauvoir: “no rechazaba la maternidad como tal, sino, sólo la maternidad como ha sido organizada en la mayor parte del siglo veinte: como, efectivamente, esclavitud”.^49^

Stone rectifica que El Segundo Sexo documenta las fuerzas biológicas, históricas y culturales que posicionan a las mujeres como un “Otro de los hombres”^50^ en cada paso de la Historia. En contraparte de los hombres, las mujeres no han gozado de la trascendencia fundamental de la existencia humana en la que los humanos “trascendemos a las situaciones autodadas con la creación de nuevos valores, metas y significados a través de los que asignamos y reformamos esas situaciones”.^51^ Para Sartre, de acuerdo con Genevieve Lloyd, la trascendencia fundamental de la existencia humana recae en que la preocupación existencial del ser humano (por la libertad absoluta) es ir más allá toda situación determinada.^52^ La mujer, a diferencia del hombre, no ha tenido (ni tiene) las mismas condiciones para poder trascender las situaciones determinadas. Por ello, en ese “Otro de los hombres” la maternidad se inserta como una ideología que la convierte en algo esencial, natural e instintivo para las mujeres. Cuando debería ser por “elección y una actividad”,^53^ nunca un fin.

Para Beauvoir, de acuerdo con Stone, la maternidad no es un fin en sí mismo que traerá felicidad absoluta a la mujer. Ya que, en algún punto, el hijo se convertirá en adulto y dejará de necesitar el cuidado de su madre. Por lo tanto, si la vida de la mujer se volcó únicamente en la maternidad, ella se sentirá infeliz e insatisfecha cuando su hijo alcance la madurez.

Entonces, teniendo en cuenta el contexto histórico en el que se escribió _El Segundo Sexo_, las experiencias vividas comunes entre las mujeres europeas y la perspectiva de Stone con respecto de la maternidad según Beauvoir, la maternidad no es un impedimento en sí misma. Más bien, lo que la vuelve un obstáculo para la emancipación femenina son las prácticas sociales, culturales e históricas a las que se ven sometidas las mujeres para ser madres. Incluso, Stone asegura que Beauvoir estaría de acuerdo si la maternidad es percibida como una práctica más en la vida femenina, se realiza con el acompañamiento de un grupo y si se toma como un compromiso libre.^54^

Así, pues, yo apostaría por intentar pensar la maternidad más allá de un impedimento cerrado a la emancipación femenina. Repensarla como un espacio de indeterminación y explorarla como acto creativo y crítico que también puede enunciarse desde otras partes de Europa. No obstante, deseo proseguir ateniéndome a la proposición beauvoiriana de evitar mistificar la maternidad, a partir de no postularla como el modo en el que la mujer se vuelve el igual del hombre. Porque, de esa manera, continuaría con el supuesto psicoanalítico de que el hijo es la compensación ante la falta de pene de la mujer.^55^ Quizá, sea más plausible apostar por replantear la maternidad desde la creatividad que podría implicar su actividad; puesto que, como Beauvoir dice, “[…] no existe ningún instinto maternal […]. La actitud de la madre es definida por el conjunto de su situación y por el modo en que la asume”.^56^

## La maternidad como cultura femenina

Rosario Castellanos con _Sobre cultura femenina_ no desvincula la maternidad de la libertad porque la considera como un acto trascendente. Sin embargo, es importante interrogar si, para ella, la maternidad sólo es creación o si tiene múltiples modos de experimentarse. Ya que, de ser sólo creación, aquel acto creativo de la maternidad reduce, de alguna forma, la libertad de las mujeres.

En _Sobre cultura femenina_, Castellanos responde a las premisas de Schopenhauer, Simmel y Weininger que subrayan el carácter inherente e inferior de las mujeres frente a los hombres. Por lo que en respuesta, retomando el sistema de Schopenhauer, ella postula que hay tres componentes de los seres humanos. El primero es el instinto; el cual se relaciona con la sensibilidad corporal y con todo proceso biológico. El segundo componente es el intelecto, es decir, la inteligencia. El último componente es el espíritu. El espíritu es “[…] considerado como forma de conocimiento, es la consciencia de la limitación, la temporalidad y la muerte, el espíritu considerado como conducta, es un intento de superación de estos obstáculos […]”.^57^ Para Castellanos, el espíritu intenta evitar la muerte, porque es consciente de su propia contingencia y el modo de evitarla es llevar a cabo un acto que le eternice.^58^

Para Castellanos, el componente instintivo, el intelectual y el espiritual trabajan conjuntamente, “[e]l espíritu, al servirse del cerebro, hace instrumento suyo todo el cuerpo”.^59^ Todavía más, para Castellanos, los componentes se influyen entre sí. Por lo que el sexo de cada cuerpo, tiene repercusiones en cada espíritu: “[y] si decimos cuerpo, decimos sexo, cuerpo de mujer, cuerpo de hombre. Es lícito pues, hablar, según el instrumento que utiliza, de un espíritu masculino y un espíritu femenino”.^60^ Por consiguiente, la pensadora propone dos modos propios para eternizarse, para cada tipo de espíritu. El primero, el del espíritu masculino, es la creación cultural. Tal como la escritura, la pintura o la música. El segundo modo de eternizarse, el femenino, es la maternidad.^61^
Parece que para Castellanos, la maternidad se constituyó como la forma para eternizarse propia de las mujeres debido a que ellas, históricamente, han sido excluidas de cualquier otro tipo de medios para hacerlo. Castellanos contempla que la maternidad “[p]ara la mujer es una perpetuación del cuerpo en el tiempo”^62^ y, así, “la maternidad como un modo de creación y perpetuación tan lícito y tan eficaz como el otro [el cultural masculino]”.^63^ Así, el motivo por el que las mujeres deben redimir la falta de creación con la cultura se sustenta con la posición de Lamas, en la que Rosario Castellanos y Simone de Beauvoir (como feministas contemporáneas) coinciden en que: “la diferencia que vemos socialmente entre las mujeres y los hombres no deriva de la biología o de una incapacidad congénita, sino de un encauzamiento distinto de la energía y el espíritu”.^64^

Sin embargo, ¿qué implica presentar a la maternidad como un modo de eternizarse con el cuerpo? En la interpretación de Lamas supone que la producción cultural en los entes masculinos, con respecto del espíritu humano, cumple la misma función que la maternidad en las mujeres debido a motivos históricos y corporales.^65^ Castellanos complementa la función corporal de gestación con el intelecto y con el despliegue del espíritu. Por lo que, para su interpretación, retomo la postulación de Gabriela Cano que indica que la maternidad “no es sencillamente una función de reproducción biológica”,^66^ sino que es un sentimiento consciente y, sobre todo, libre.

Haciendo un recuento de lo explicado hasta ahora, Rosario Castellanos con _Sobre Cultura femenina_, señala: 1) el espíritu humano se eterniza a través del cuerpo, 2) el espíritu masculino lo hace con la creación cultural, el femenino con la maternidad y 3) la maternidad es “una alternativa de trascendencia tan […] satisfactoria como la cultura lo es para los hombres”,^67^ pero no como una mera resolución biológica, sino como un “equivalente espiritual a la creación cultural masculina”.^68^ En este punto, quizá, para “espíritu” sirva como símil deseo de hombres y de mujeres y con “eternización” un acto trascendente.

Empero ¿la tercera premisa no estaría mistificando la maternidad como había advertido Beauvoir en _El Segundo Sexo_? En este punto, para contestar a tal pregunta, encuentro útil recurrir a la lectura de _Sobre cultura femenina_ de Larisch.^69^ Para ella, “la ética existencial y la maternidad del Segundo Sexo se entrelazan de manera distinta al análisis ético y de la maternidad en Sobre cultura femenina”.^70^ Porque, en la lectura de Larisch del Segundo Sexo, el modo en el que opera la libertad dentro de la maternidad es nulo, en tanto que la madre no está en la misma condición que los hombres de “consentir o rechazar la obligación [de la maternidad] en relación con el bien público”.^71^ Incluso, si se le concede cierta creación a la maternidad, para Beauvoir se haría dentro de la contingencia y la facticidad por lo que quedaría dentro de la inmanencia. Es decir, la maternidad constituiría (de acuerdo con Larisch) una obligación social de la mujer que no es consensual. Larisch sostiene que Rosario Castellanos no introdujo esta premisa en su tesis porque muy probablemente no leyó, en ese entonces, _El Segundo Sexo_ y, por consiguiente, construyó su propio entramado filosófico.

Por su parte, en la interpretación de Larisch de _Sobre cultura femenina_, la maternidad se explica desde una posición marginal que tiene como posibilidad de enunciación la cultura que es designada por los hombres. Larisch destaca que Castellanos aborda la maternidad desde la masculinidad debido a que: “[m]odelada desde la cultura masculina, ella [Castellanos] debe observar «la conducta masculina» para aproximarse al problema del «otro» en la cultura”.^72^ Es decir, Castellanos al ser definida dentro de una cultura masculinizada en todos sus aspectos, decide pensar la maternidad desde la perspectiva masculina en la que está sólidamente inmersa y, por consiguiente, termina masculinizando aquello que en un principio escapaba a la injerencia masculina en tanto que la maternidad se articula como un espacio solo de mujeres. 

Incluso, para explicar por qué algunas mujeres crean cultura en lugar de tener descendencia, Castellanos plantea que ellas componen como un sustituto de los hijos, “trayendo a cuestas sus sentimientos maternales frustrados”.^73^ Es decir, las mujeres, al no tener hijos, intentan producir arte sin poseer las habilidades necesarias para crear algo auténtico que las “eternice”. Castellanos, destaca que las obras femeninas, por esta falta, terminan mimetizando las obras de arte de los varones.^74^ Entonces cabe preguntar si ¿Castellanos también está cerrando la experiencia materna a un modo de experiencia?

## Conclusión: Maternidades creativas e indeterminadas

Tanto Castellanos como Beauvoir proponen, con referentes unidos a la masculinidad, un modo de conceptualizar y evaluar la maternidad. Lo que hace cuestionarse de qué manera se presentaría la libertad en la maternidad y cuáles podrían ser los límites dentro de los que las madres tendrían que buscar afirmarse a sí mismas.

Por un lado, Beauvoir destaca fuertemente dos nociones sobre la maternidad. La primera se relaciona con los dos sentimientos encontrados que se desarrollan durante la gestación. Uno de ellos da la sensación de que el feto se gesta, dentro de la mujer, igual que un parásito. El segundo sentimiento ofrece que el feto sí es parte de la madre y, por consiguiente, no debería ser repudiado por ella.^75^ La segunda noción destacada por Beauvoir es el imperativo social de cumplir con determinadas expectativas. Tal como lo sería el amor incondicional hacia los hijos.^76^

Gracias a las dos nociones sostenidas, se podría interpretar que Beauvoir tiende hacia una lectura de la maternidad más referida a un obstáculo. Es decir, hacia algo que reduce la libertad de la mujer. _El Segundo Sexo_ (desde el existencialismo) analiza cómo la condición femenina se distancia de los aspectos emancipatorios, no por la diferencia de sexos, sino por las condiciones históricas y materiales en las que se inscriben las mujeres reduciendo su igualdad de condición frente a los hombres. La maternidad, por ello, se presenta como un fenómeno “inmanente”, es decir, que se hace difícil pensar la libertad conjuntamente con la maternidad, esto porque se parte de afirmar que la emancipación debe ser transcendente de manera que se continue superando las metas que uno mismo se ha impuesto. 

Por otro lado, _Sobre cultura femenina_ apunta a que la maternidad ha sido la manera en la que las mujeres han podido afirmarse a sí mismas. Es decir, en la terminología de Castellanos, “eternizan” sus cuerpos sobre el tiempo y la tierra: “[l]a mujer satisface su necesidad de eternizarse por medio de la maternidad y perpetúa, a través de ella, la vida en el cuerpo, cuerpo sobre la tierra”.^77^ Esto se debe a que, históricamente, las mujeres han sido excluidas de la creación cultural y se vieron en la necesidad de apropiarse conscientemente de su función reproductiva como su modo originario y auténtico de crear.^78^ En este punto, Castellanos compara la creación cultural masculina con la maternidad. _Sobre cultura femenina_ se pregunta si existe algo llamado “cultura femenina”, es decir, si hay una creación cultural de las mujeres que sea equivalente a la creación cultural dominada por los hombres. Castellanos concluye que la exclusión histórica de las mujeres de la cultura, las ha forzado a crear sólo en la maternidad, y a través de ella. Por lo que la maternidad, retomando el lenguaje de Schopenhauer, trasciende el “espíritu” hacia la “eternización”. Por lo que, parece propicio conjeturar que, también, Castellanos entiende la libertad referida a la trascendencia. Sin embargo, a distinción de Beauvoir, la maternidad para ella sí es trascendente.

A pesar de que los parámetros de Castellanos, son concebidos para responder a la exclusión cultural femenina y reemplazar la falta de creación producida por ella, parecen esencializar la maternidad como el modo más propio y auténtico de las mujeres después de que son postulados. Incluso, ya suponiendo que la maternidad es lo trascendente para las mujeres, Castellanos llega a sugerir, de pasada, que las mujeres, cuando optan por “sublimar los instintos”^79^ maternos en la cultura (escribiendo, pintando o en cualquier otra forma de arte), se “apartan” “de su feminidad confinándola a una mimetización del varón”.^80^ La afirmación de Castellanos da la sensación de que la experiencia de la creación cultural hace que la mujer no sea original ni trascendente, sino “un mero producto de una frustración [la de no poder concebir hijos]”.^81^ Por ello, la maternidad toma un carácter que, en cierto modo, también reduce la libertad de los múltiples modos de experimentarla. Se vuelve imperante que la maternidad sea experimentada de modo trascendente y creativo, adoptando una posición experiencial cerrada en un ámbito que no había sido capturado por el dominio masculino.

Así, la maternidad parece necesitar de una postulación que combine una noción trascendente y una inmanente. Por ello, deseo recoger una maternidad creativa, mas no esencialista. En tanto que la creatividad parece ser la manera de indeterminar los modos de experiencia y la forma en la que la maternidad puede ser inmanente o trascendente dependiendo cuál de las dos acerca a la madre, por decisión propia, hacia su libertad.

No obstante, también considero la posibilidad de que la maternidad como creación, pensada por Castellanos, realmente no se pueda empatar con la indeterminación (con la libertad) que Beauvoir busca para las mujeres. En otras palabras, preguntarme si las postulaciones de Castellanos y Beauvoir realmente son compatibles. Si los planteamientos de la maternidad como trascendente e inmanente, respectivamente, difieren tanto que impiden, por completo, pensarlas bajo una misma línea.

Apuesto por que la creación da una condición de apertura para las mujeres de hacer materia sus decisiones; de forma que, en cierto sentido, se da origen a un vínculo con la libertad. Este vínculo, si bien posee muchas restricciones y percances, radica en la afirmación de las madres, de sus cuerpos y de sus desiciones con respecto de la demanda de cuidados y de la gestación, tal como propone Castellanos. Por consiguiente reafirmo la actividad materna como creatividad y como un posible espacio donde la multiplicidad se permite, donde la libertad se hace presente aunque sea por unos instantes. En tanto que la maternidad es contingente y está dentro de la facticidad, tal como destaca Beauvoir.

En resumen, la maternidad creativa de Castellanos (empatada con la perspectiva crítica de Beauvoir), abre la posibilidad de que las mujeres se afirmen (incluyendo su cuerpo) dentro de condiciones que, en algún punto, también representan la reducción de su agencia. Es decir, cuando la posible gestación y la demanda de cuidado se presenten ya no como una imposición social basada en la desigualdad de género y ya no, necesariamente, implicarán una limitación infeliz en la libertad de la madre. Como punto de encuentro, Castellanos y Beauvoir destacarían la contingencia de la maternidad y, quizá por ello, una abertura para la multiplicidad de modos de experiencia.

## Anexo, notas complementarias

^1^ BBC Mundo, “Las madres que se arrepienten de haber tenido hijos”, _BBC News_ (abril 2018): https://www.bbc.com/mundo/noticias-43628645 Fecha de consulta: 26 de abril de 2020.

^2^ Universidad Autónoma de Madrid, _Diccionario Akal de Filosofía_, (Madrid: Ediciones Akal, 2004), 250.

^3^ Sandra Reineke, “The Intellectual and Social Context of The Second Sex”,  _A companion to Simone de Beauvoir_, (Oxford: John Wiley & Sons Ltd, 2017), 29.

^4^ Un ejemplo de la aplicación de estas políticas públicas es la ejecución de la filósofa Marie‐Louise Giraud acusada de crimen contra el Estado en 1943. Giraud abortó ilegalmente para continuar con su carrera profesional y evitar dedicarse al cuidado materno. Puesto que los jefes de esa época solían despedir a las mujeres que se ausentaban debido a sus partos o por cuidar a sus niños. Este ejemplo fue ofrecido por Sandra Reineke, “The Intellectual and Social Context of The Second Sex”, 28-30.

^5^ Marta Lamas, “Rosario Castellanos, feminista a partir de sus propias palabras”, _Liminar_ vol.15, núm. 2, (julio–diciembre 2017). http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S1665-80272017000200035#aff1 Consultado el 24 de Marzo 2020.

^6^ Marta Lamas, “Rosario Castellanos, feminista a partir de sus propias palabras”. http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S1665-80272017000200035#aff1 Consultado el 24 de Marzo 2020.

^7^ Simone de Beauvoir, _El segundo sexo_, trad. Pablo Palant (Ciudad de México: Alianza Editorial Mexicana, 1989), 255.

^8^ Rosario Castellanos, _Sobre cultura femenina_, (Ciudad de México: Fondo de Cultura Económica, 2005), 216.

^9^ Castellanos, _Sobre cultura femenina_, 214.

^10^ Celia Amorós, “El Método en Simone de Beauvoir. Método y Psicoanálisis Existencial”, _Ágora_ en vol. 28, núm.1 (2009), 14.

^11^ Amorós, “El Método en Simone de Beauvoir”, 12.

^12^ Amorós, “El Método en Simone de Beauvoir”, 13.

^13^ Amorós, “El Método en Simone de Beauvoir”, 13.

^14^ Amorós, “El Método en Simone de Beauvoir”,, 13.

^15^ Amorós, “El Método en Simone de Beauvoir”, 14.

^16^  Amorós, “El Método en Simone de Beauvoir”, 14.

^17^ “[A]bout herself and had realized that in order to do so, she had to understand «the nature of women’s lives in general»” con Sandra Reineke, “The Intellectual and Social Context of The Second Sex”, _A companion to Simone de Beauvoir_, (Oxford: John Wiley & Sons Ltd, 2017). 32.

^18^ Sandra Reineke, “The Intellectual and Social Context of The Second Sex”. 3.

^19^ Sandra Reineke, “The Intellectual and Social Context of The Second Sex”, 32.

^20^ Teresa López Pardina, “Prólogo a la edición española”, _El segundo sexo_ (Madrid: Ediciones Cátedra, 2005), 5-25.

^21^ Simone de Beauvoir, _El segundo sexo_, (Ciudad de México: Alianza Editorial Mexicana, 1989), 15.

^22^ Para dar cuenta de esta 'experiencia vivida' común a las madres, Simone de Beauvoir recurre al relato de Sofía Tolstoi (descrito en su libro Diario) y al testimonio de la señora H. N. (recopilado en el libro Stekel) con Beauvoir, _El segundo sexo_, 267.

^23^ Beauvoir, _El segundo sexo_, 267.

^24^ Beauvoir, _El segundo sexo_, 267.

^25^ Beauvoir, _El segundo sexo_, 268.

^26^ Beauvoir, _El segundo sexo_, 268.

^27^ Amorós, “El Método en Simone de Beauvoir”, 17.

^28^ Beauvoir, _El segundo sexo_, 266.

^29^ Beauvoir, _El segundo sexo_, 284. 

^30^ Beauvoir, _El segundo sexo_, (Ciudad de México: Penguin Random House Grupo Editorial, 2016), 480.

^31^ Teresa López Pardina, “Prólogo a la edición española”, 9.

^32^ Beauvoir, _El segundo sexo_, 270.

^33^ Beauvoir, _El segundo sexo_, 275 y 277.

^34^ Beauvoir, _El segundo sexo_, 276-279.

^35^ Beauvoir, _El segundo sexo_, 279. Beauvoir desarrolla más: “[e]l placer que siente el hombre a lado de las mujeres (sentirse absolutamente superior) sólo es conocido por la mujer al lado de sus hijos, y sobre todo de sus hijas, y se siente frustrada si tiene que renunciar a sus privilegios de autoridad. Ya se trate de una madre apasionada u hostil […]”. (Beauvoir, 1989, 289).

^36^ Beauvoir, _El segundo sexo_, 283.

^37^ Beauvoir, _El segundo sexo_, 284

^38^ Beauvoir, _El segundo sexo_, 289.

^39^ Beauvoir, _El segundo sexo_, 298.

^40^ Linda Zerilli, “A Process without a Subject: Simone de Beauvoir and Julia Kristeva on Maternity”, _Sings_ vol. 18, núm.1, (otoño 1992): 113.

^41^ Zerilli, “A Process without a Subject”, 111.

^42^ Zerilli, “A Process without a Subject”, 112.

^43^ Lopéz Pardina subraya que a diferencia de Sartre, Beauvoir entiende la libertad referida las situaciones que rodean a los sujetos y que pueden aumentar o disminuir sus posibilidades de realizarse como libres. López Pardina, “Prólogo a la edición española”, 8.

^44^ Linda Zerilli, “A Process without a Subject”, 132.

^45^ Refiero puntualmente a este texto: Kierran Argent Horner, “Intersubjectivity in the pregnant self: maternity from Simone de Beauvoir’s The Second Sex, through Agnès Varda’s L’Opéra Mouffe to contemporary feminist thought”, _Studies in European Cinema_, (noviembre 2018): 1-18.

^46^ Horner presenta el término “attitudes” en Horner, “Intersubjectivity in the pregnant self”, 5.

^47^ Horner, “Intersubjectivity in the pregnant”, 5.

^48^ Con “percances sociales” me refiero a la restringida elección de la maternidad, en el olvido de las madres de su propia subjetividad para dedicarse única y obligadamente al cuidado del otro y de su falta de injerencia.

^49^ Alison Stone, “Beauvoir and the Ambiguities of Motherhood”, _A Companion to Simone De Beauvoir_, (Oxford: John Wiley & Sons Ltd, 2017), 122.

^50^ Stone, “Beauvoir and the Ambiguities of Motherhood”, 123.

^51^ Esta premisa es retomada del existencialismo francés de acuerdo con Stone, “Beauvoir and the Ambiguities of Motherhood”, 123.

^52^ Genevieve Lloyd. “Masters, Slaves and Others”, _Radical Philosophy_ RP 034, (verano 198) https://www.radicalphilosophy.com/article/masters-slaves-and-others Consultado el 4 de marzo del 2020.

^53^ Stone, “Beauvoir and the Ambiguities of Motherhood”, 124.

^54^  Stone, “Beauvoir and the Ambiguities of Motherhood”. 125.

^55^ Beauvoir, _El Segundo Sexo_, 297. 

^56^ Beauvoir, _El Segundo Sexo_, 282.

^57^ Castellanos, _Sobre cultura femenina_, 166.

^58^ Castellanos, _Sobre cultura femenina_, 168.

^59^ Castellanos, _Sobre cultura femenina_, 173.

^60^ Castellanos, _Sobre cultura femenina_, 173.

^61^ Castellanos, _Sobre cultura femenina_, 174.

^62^ Castellanos, _Sobre cultura femenina_, 189.

^63^ Castellanos, _Sobre cultura femenina-, 192.

^64^ Lamas, “Rosario Castellanos, feminista a partir de sus propias palabras”. http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S1665-80272017000200035#aff1 Consultado el 24 de Marzo 2020.

^65^ Lamas, “Rosario Castellanos, feminista a partir de sus propias palabras”. http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S1665-80272017000200035#aff1 Consultado el 24 de Marzo 2020

^66^ Gabriela Cano, “Rosario Castellanos: entre preguntas estúpidas y virtudes locas”, _Debate Feminista_ vol. 6, (septiembre 1992). 256.

^67^ Cano,“Rosario Castellanos: entre preguntas estúpidas y virtudes locas”, 255.

^68^ Cano,“Rosario Castellanos: entre preguntas estúpidas y virtudes locas”, 256.

^69^ Sharon Larisch, “Ethics, Eros, and Necessity: Rosario Castellanos on the Two Simones”, _Chasqui,_ vol. 39, núm. 1, (2010): 104-119.

^70^ Larisch, “Ethics, Eros, and Necessity”, 106.

^71^ Larisch, “Ethics, Eros, and Necessity”, 107.

^72^ Larisch, “Ethics, Eros, and Necessity”, 108.

^73^  Castellanos, “Sobre cultura femenina”, 209.

^74^ Castellanos, “Sobre cultura femenina”, 201 y 214.

^75^ Simone de Beauvoir constata que la comunidad médica subraya “que el feto no pertenece a la madre sino que es un ser autónomo. Sin embargo, esos mismos médicos «bien pensados» afirman que el feto forma parte del cuerpo materno, y que no es un parásito que se nutre a su costa”. Con este tipo de aseveraciones, Beauvoir, _El Segundo Sexo_, 256, destaca la presión a las que las madres se someten. 

^76^ Este amor condicional puede llegar a impulsar ciertas premisas que subrayan la voluntad de los hijos por encima de los de la madre. Tal como sería el consejo que la comunidad médica ofrece a una mujer en caso de que requiera optar por conservar una sola vida (madre o feto). Beauvoir, _El Segundo Sexo_, 255.

^77^ Rosario Castellanos, _Sobre cultura femenina_. (Ciudad de México: Fondo de Cultura Económica, 2005). 216.

^78^ Castellanos, _Sobre cultura femenina_, 216.

^79^ Castellanos, _Sobre cultura femenina_, 202.

^80^ Castellanos, _Sobre cultura femenina_, 214.

^81^ Castellanos, _Sobre cultura femenina_, 216.

## Referencias

Amorós, Celia. “El Método en Simone de Beauvoir. Método y Psicoanálisis Existencial”. En Ágora en vol. 28, núm.1 (2009): 11-29.

Castellanos, Rosario. Sobre cultura femenina. Ciudad de México: Fondo de Cultura Económica 2005.

Cano, Gabriela. “Rosario Castellanos: entre preguntas estúpidas y virtudes locas”. En Debate Feminista vol. 6, (septiembre 1992). 256.

De Beauvoir, Simone. El Segundo Sexo. Trad. Pablo Palant. Ciudad de México: Alianza Editorial Mexicana 1989.

De Beauvoir, Simone. El Segundo Sexo. Trad. Juan García Puente. Ciudad de México: Penguin Random House Grupo Editorial 2016.

Horner, Kierran Argent. “Intersubjectivity in the pregnant self: maternity from Simone de Beauvoir’s The Second Sex, through Agnès Varda’s L’Opéra Mouffe to contemporary feminist thought”. En Studies in European Cinema, (Noviembre 2018): 1-18. DOI: 10.1080/17411548.2018.1531502. 

Lamas, Marta. “Rosario Castellanos, feminista a partir de sus propias palabras” en Liminar vol.15, núm. 2, (julio–diciembre 2017). http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S1665-80272017000200035#aff1

Larisch, Sharon. “Ethics, Eros, and Necessity: Rosario Castellanos on the Two Simones” en Chasqui, vol. 39, núm. 1, (2010): 104-119.

Lloyd, Genevieve. “Masters, Slaves and Others”. En Radical Philosophy RP 034, (verano 198) https://www.radicalphilosophy.com/article/masters-slaves-and-others. Consultado el 4 de marzo del 2020.

López Pardina, Teresa. “Prólogo a la edición española”. En El Segundo Sexo.Trad. 
Alicia Martorell. Madrid: Ediciones Cátedra 2005. 5-24.

Reineke, Sandra. “The Intellectual and Social Context of The Second Sex”. En A companion to Simone de Beauvoir. Eds. Laura Hengehold y Nancy Bauer. Oxford: John Wiley & Sons Ltd, 2017. 28-36.

Stone, Alison. “Beauvoir and the Ambiguities of Motherhood”. En A Companion to Simone De Beauvoir. Eds. Laura Hengehold y Nancy Bauer. Oxford: John Wiley & Sons Ltd 2017. 122-133.

Universidad Autónoma de Madrid. Diccionario Akal de Filosofía. Madrid: Ediciones Akal 2004.

Zerilli, Linda. “A Process without a Subject: Simone de Beauvoir and Julia Kristeva on Maternity”. En _Signs_ en vol. 18, núm.1, (otoño 1992): 111-135.